package com.egame.soundrecord;

public class StorageCommon {
    private boolean isScanFrg = true;

    public boolean isScanFrg() {
        return isScanFrg;
    }

    public void setScanFrg(boolean scanFrg) {
        isScanFrg = scanFrg;
    }
}
