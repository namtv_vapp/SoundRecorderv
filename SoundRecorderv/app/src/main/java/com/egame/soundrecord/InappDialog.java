package com.egame.soundrecord;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.TextView;

import com.egame.soundrecorder.R;


public class InappDialog extends Dialog {

    public InappDialog(@NonNull Context context) {
        super(context, R.style.AppTheme);
        setContentView(R.layout.dialog_inapp);
    }

    private ICallback callback;

    public void setCallback(ICallback callback) {
        this.callback = callback;
    }

    @Override
    public void show() {
        super.show();
        initView();
    }

    private void initView() {
        ((TextView) findViewById(R.id.tv_price)).setText(Pucharse.getInstance(getContext()).getPrice());
        findViewById(R.id.iv_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        findViewById(R.id.tv_pucharse).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callback.onInAppPurchase();
            }
        });

        TextView tvOldPrice = findViewById(R.id.tv_old_price);
        tvOldPrice.setText(Pucharse.getInstance(getContext()).getOldPrice());
        tvOldPrice.setPaintFlags(tvOldPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
    }

    public interface ICallback {
        void onInAppPurchase();
    }
}
