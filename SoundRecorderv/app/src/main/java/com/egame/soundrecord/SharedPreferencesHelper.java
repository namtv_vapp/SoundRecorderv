package com.egame.soundrecord;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import com.egame.soundrecord.App;

public class SharedPreferencesHelper {
    private static final String FILE_SETTING = "setting.pref";
    private static final String IS_PURCHASE = "IS_PURCHASE";

    public static void setPurchased(Activity activity, boolean isPurcharsed) {
        SharedPreferences pref = activity.getSharedPreferences(FILE_SETTING, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(IS_PURCHASE, isPurcharsed);
        editor.apply();
    }

    public static boolean isPurchased(Activity activity) {
        return activity.getSharedPreferences(FILE_SETTING, Context.MODE_PRIVATE).getBoolean(IS_PURCHASE, false);
    }
}
