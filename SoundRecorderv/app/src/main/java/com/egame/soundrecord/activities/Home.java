package com.egame.soundrecord.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.RequiresApi;
import android.view.View;


import com.egame.soundrecord.Helper;
import com.egame.soundrecord.Pucharse;
import com.egame.soundrecorder.R;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;

import java.util.Timer;
import java.util.TimerTask;

public class Home extends Activity {
    private InterstitialAd mInterstitialAd;
    private Timer waitTimer;
    private boolean interstitialCanceled;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home);
        //

        try {
            Pucharse.getInstance(this).initBilling();
            if (Pucharse.getInstance(Home.this).isPucharsed()) {
                Helper.displayAds = 0;
            } else {
                Helper.displayAds = 1;
            }
        }
        catch (Exception exxx)
        {
            Helper.displayAds = 0;
        }
        if(Helper.displayAds == 1) {
            showInterstitialAd();
        }
        else
        {
            Intent mainIntent = new Intent(Home.this, MainActivity.class);
            startActivity(mainIntent);
        }
    }

    private void startMainActivity() {
        waitTimer.cancel();
        interstitialCanceled=true;
        finish();

        Intent mainIntent = new Intent(Home.this, MainActivity.class);
        startActivity(mainIntent);
    }

    private void showInterstitialAd () {
        final Intent i = new Intent(Home.this, MainActivity.class);
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getString(R.string.interstitial_ad_unit_id));
        runOnUiThread(new Runnable() {
            @Override public void run() {
                mInterstitialAd.loadAd(new AdRequest.Builder()
                        .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                        .addTestDevice("3C94990AA9A387A256D3B2BBBFEA51EA")
                        .addTestDevice("6F599887BC401CFB1C7087F15D7C0834")
                        .addTestDevice("B543DCF2C7591C7FB8B52A3A1E7138F6")
                        .addTestDevice("8619926A823916A224795141B93B7E0B")
                        .addTestDevice("6399D5AEE5C75205B6C0F6755365CF21")
                        .addTestDevice("2E379568A9F147A64B0E0C9571DE812D")
                        .addTestDevice("A0518C6FA4396B91F82B9656DE83AFC7")
                        .addTestDevice("C8EEFFC32272E3F1018FC72ECBD46F0C")
                        .build());
            }
        });

        mInterstitialAd.setAdListener(new AdListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onAdLoaded() {
                if(mInterstitialAd.isLoaded())
                {
                    startActivity(i);
                    mInterstitialAd.show();
                }

            }
            @Override
            public void onAdFailedToLoad(int errorCode) {
                startActivity(i);
                //overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
            @Override
            public void onAdOpened() {
                // Code to be executed when the ad is displayed.
            }
            @Override
            public void onAdClicked() {
                // Code to be executed when the user clicks on an ad.
                //Toast.makeText(ActivitySplash.this, "666666", Toast.LENGTH_SHORT).show();
                startActivity(i);
                //overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }
            @Override
            public void onAdClosed() {
                //overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });
    }
    @Override
    public void onResume() {
        super.onResume();
        try {
            Pucharse.getInstance(this).initBilling();
            if (Pucharse.getInstance(Home.this).isPucharsed()) {
                Helper.displayAds = 0;
            } else {
                Helper.displayAds = 1;
            }
        }
        catch (Exception exxx)
        {
            Helper.displayAds = 0;
        }
        if (Helper.exxistApp == 1) {
            Helper.exxistApp = 0;
            finish();
        }
    }
}
