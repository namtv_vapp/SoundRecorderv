package com.egame.soundrecord;

import android.app.Application;

public class App extends Application {
    private static App instance;

    protected StorageCommon storageCommon;

    public StorageCommon getStorageCommon() {
        return storageCommon;
    }

    public static App getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        storageCommon=new StorageCommon();
    }
}
