package com.egame.soundrecord.fragments;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.os.FileObserver;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.egame.soundrecord.DBHelper;
import com.egame.soundrecord.Helper;
import com.egame.soundrecord.RecordingItem;
import com.egame.soundrecorder.R;
import com.egame.soundrecord.adapters.FileViewerAdapter;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.VideoController;
import com.google.android.gms.ads.VideoOptions;
import com.google.android.gms.ads.formats.MediaView;
import com.google.android.gms.ads.formats.NativeAdOptions;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.gms.ads.formats.UnifiedNativeAdView;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Daniel on 12/23/2014.
 */
public class FileViewerFragment extends Fragment{
    private static final String ARG_POSITION = "position";
    private static final String LOG_TAG = "FileViewerFragment";
    private int timeCallAds = 0;
    private int position;
    private FileViewerAdapter mFileViewerAdapter;
    private UnifiedNativeAd nativeAd;
    private Timer waitTimer;
    public static FileViewerFragment newInstance(int position) {
        FileViewerFragment f = new FileViewerFragment();
        Bundle b = new Bundle();
        b.putInt(ARG_POSITION, position);
        f.setArguments(b);

        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        position = getArguments().getInt(ARG_POSITION);
        observer.startWatching();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_file_viewer, container, false);

        RecyclerView mRecyclerView = v.findViewById(R.id.recyclerView);
        mRecyclerView.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);

        //newest to oldest order (database stores from oldest to newest)
        llm.setReverseLayout(true);
        llm.setStackFromEnd(true);

        mRecyclerView.setLayoutManager(llm);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        if(Helper.displayAds == 1) {
            mFileViewerAdapter = new FileViewerAdapter(getActivity(), llm, showIntersAd());
        }
        else
        {
            mFileViewerAdapter = new FileViewerAdapter(getActivity(), llm, mInterstitialAd);
        }
        mRecyclerView.setAdapter(mFileViewerAdapter);
        if(Helper.displayAds == 1) {
            refreshAd(v);
        }
        return v;
    }

    InterstitialAd mInterstitialAd;

    private InterstitialAd showIntersAd () {
        mInterstitialAd = new InterstitialAd(mActivity);
        mInterstitialAd.setAdUnitId(getResources().getString(R.string.interstitial_ad_unit_id));
        AdRequest adRequestInter = new AdRequest.Builder()
                .addTestDevice(getString(R.string.tai_device_id))
                .addTestDevice(getString(R.string.huyen_device_id))
                .addTestDevice(getString(R.string.nam_device_id))
                .build();
        mInterstitialAd.loadAd(adRequestInter); //todo enable in production

        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                super.onAdClosed();

            }

            @Override
            public void onAdLoaded() {
            }

            public void onAdClicked() {
            }

            public void onAdFailedToLoad(int var1) {
            }
        });

        return mInterstitialAd;
    }

    FileObserver observer =
            new FileObserver(android.os.Environment.getExternalStorageDirectory().toString()
                    + "/SoundRecorder") {
                // set up a file observer to watch this directory on sd card
                @Override
                public void onEvent(int event, String file) {
                    if(event == FileObserver.DELETE){
                        // user deletes a recording file out of the app

                        String filePath = android.os.Environment.getExternalStorageDirectory().toString()
                                + "/SoundRecorder" + file + "]";

                        Log.d(LOG_TAG, "File deleted ["
                                + android.os.Environment.getExternalStorageDirectory().toString()
                                + "/SoundRecorder" + file + "]");

                        // remove file from database and recyclerview
                        mFileViewerAdapter.removeOutOfApp(filePath);
                    }
                }
            };

    Activity mActivity;
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (Activity) context;
    }

    private void refreshAd(final View v) {
        timeCallAds = timeCallAds + 1;
        if(Helper.unifiedNativeAd != null) {
            FrameLayout frameLayout = v.findViewById(R.id.fl_ad_file_view);
            UnifiedNativeAdView adView = (UnifiedNativeAdView) mActivity.getLayoutInflater()
                    .inflate(R.layout.ad_undefind_list, null);
            Helper.populateUnifiedNativeAdView_banner(Helper.unifiedNativeAd, adView);
            if(frameLayout != null) {
                frameLayout.removeAllViews();
                frameLayout.addView(adView);
            }
        }
        else{
            if(timeCallAds < 4) {
                waitTimer = new Timer();
                waitTimer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                refreshAd(v);
                            }
                        });
                    }
                }, 2000);
            }

        }
    }
}




